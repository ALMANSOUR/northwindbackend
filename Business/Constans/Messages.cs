﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Entities.Concrete;

namespace Business.Constans
{
    public static class Messages
    {
        public static string ProductAdded = "Ürün Başarıyla Eklendi";
        public static string ProductDeleted = "Ürün Başarıyla Silindi";
        public static string ProductUpdated = "Ürün Başarıyla Güncellendi";
        public static string ProductNameAlreadyExists = "Ürün İsmi zaten Mevcüt";

        public static string CategoryAdded = "Category Başarıyla Eklendi";
        public static string CategoryDeleted = "Category Başarıyla Silindi";
        public static string CategoryUpdated = "Category Başarıyla Güncellendi";

        public static string UserNotFound = "Kullanıcı Bulunamadı";
        public static string PasswordError = "Şifre Hatalı";
        public static string SuccessfulLogin="Login Başarılı";
        public static string UserAlreadyExists="bu kullanıcı zaten mevcut";
        public static string UserRegistered = "kullanıcı başarıyla kaydedildi";

        public static string AccessTokenCreated = "Access token başarıyla oluşturuldu";

        public static string AuthorizationDenied = "Yetkiniz Yok";
    }
}
