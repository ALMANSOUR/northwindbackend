﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities.Dtos;
using FluentValidation;

namespace Business.ValidationRules.FluentValidation
{
    public class UserForLoginValidator : AbstractValidator<UserForLoginDto>
    {
        public UserForLoginValidator()
        {
            RuleFor(p => p.Email).NotEmpty().WithMessage("Mail Adresi Gereklidir");
            RuleFor(p => p.Password).NotEmpty().WithMessage("Password Gereklidir");
        }
    }
}
