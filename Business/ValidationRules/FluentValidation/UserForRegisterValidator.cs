﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities.Dtos;
using FluentValidation;

namespace Business.ValidationRules.FluentValidation
{
    public class UserForRegisterValidator : AbstractValidator<UserForRegisterDto>
    {
        public UserForRegisterValidator()
        {
            RuleFor(p => p.FirstName).NotEmpty().WithMessage("FirstName Gereklidir");
            RuleFor(p => p.LastName).NotEmpty().WithMessage("LastName Gereklidir");
            RuleFor(p => p.Email).NotEmpty().WithMessage("Mail Adresi Gereklidir");
            RuleFor(p => p.Email).EmailAddress().WithMessage("Mail Adresi Geçerli Değil");
            RuleFor(p => p.Password).NotEmpty().WithMessage("Password Gereklidir");
            //en az bir büyük harf, en az bir küçük harf, en az bir rakam, en az bir özel harf "\" hariç, en az 8 uzunluğa sahip olmalı
            RuleFor(p => p.Password).Matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])(?=.{8,})").WithMessage("Password Geçerli Değil");
        }
    }
}
