﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Schema;
using Business.Abstract;
using Business.BusinessAspects.Autofac;
using Business.Constans;
using Business.ValidationRules.FluentValidation;
using Core.Aspects.Autofac.Caching;
using Core.Aspects.Autofac.Logging;
using Core.Aspects.Autofac.Performance;
using Core.Aspects.Autofac.Transaction;
using Core.Aspects.Autofac.Validation;
using Core.CrossCuttingConcerns.Logging.Log4Net.Loggers;
using Core.CrossCuttingConcerns.Validation;
using Core.Utilities.Business;
using Core.Utilities.IoC;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Business.Concrete
{
    public class ProductManager : IProductService
    {
        private readonly IProductDal _productDal;
        //başka repository çağırmak için örneğim category: ICategoryDal çağırmamalıyız, ICategoryService çağırmalıyız ve içinde kategori ile ilgili tüm kodlar yazılır.
        private ICategoryService _categoryService;
        private IAuthService _authService;
        public ProductManager(IProductDal productDal, ICategoryService categoryService, IAuthService authService)
        {
            _productDal = productDal;
            _categoryService = categoryService;
            _authService = authService;
        }

        //priority öncelik sıralaması için kullanılır, bir kaç tane attribute varsa önemlidir.
        [ValidationAspect(typeof(ProductValidator), Priority = 1)]
        //yeni bir record eklendiğinde cache silinir. Burada sadece Get işlemin cachei silinmiyor, get içeren her operasyon, örneğim GetById,GetList,GetListByCategory...
        [CacheRemoveAspect(pattern: "IProductService.Get", Priority = 2)]
        public IResult Add(Product product)
        {
            //Business Rules kullanım sebebimiz, Add içinde diğer işlemler ve fonksiyonlar çağıracaz ama gidip tek tek kontrol edeceğimize tek bir yerde kontrol ederiz. hem code daha kısa hem de daha sağlıklı sonuç olur.
            IResult result =BusinessRules.Run(CheckIfProductNameExists(product.ProductName),CheckIfCategoryIsEnabled());
            if (result != null)
            {
                return result;
            }
            _productDal.Add(product);
            return new SuccessResult(Messages.ProductAdded);
        }

        private IResult CheckIfProductNameExists(string productName)
        {
            if (_productDal.Get(p => p.ProductName == productName) != null)
            {
                return new ErrorResult(Messages.ProductNameAlreadyExists);
            }

            return new SuccessResult();
        }

        //bu method sadece category service kullanımı göstermek amaçlı, gerçek iş olarak hiç bir önemi yok.
        private IResult CheckIfCategoryIsEnabled()
        {
            var result = _categoryService.GetList();
            if (result.Data.Count<10)
            {
                return new ErrorResult(Messages.ProductNameAlreadyExists);
            }

            return new SuccessResult();
        }
        public IResult Delete(Product product)
        {
            _productDal.Delete(product);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public IDataResult<Product> GetById(int productId)
        {
            return new SuccessDataResult<Product>(_productDal.Get(p => p.ProductId == productId));
        }

        [PerformanceAspect(5)]
        public IDataResult<List<Product>> GetList()
        {
            Thread.Sleep(5000);
            return new SuccessDataResult<List<Product>>(_productDal.GetList().ToList());
        }

        [SecuredOperation("Product.List,Admin")]
        [CacheAspect(duration: 10)]
        [LogAspect(typeof(FileLogger))]
        public IDataResult<List<Product>> GetListByCategory(int categoryId)
        {
            var pr1 = _productDal.Get(x=>x.ProductId==2);
            var pr2 = _productDal.Get(x=>x.ProductId==2,c=>c.Category);
            var pr3 = _productDal.Get(x=>x.ProductId==2,"Category");
            var pr4 = _productDal.GetList();
            var pr5 = _productDal.GetList(x=>x.CategoryId==1);
            var pr6 = _productDal.GetList(x=>x.CategoryId==1,c=>c.Category);
            var pr7 = _productDal.GetList(includeProperties:c=>c.Category);
            var pr8 = _productDal.GetList(includeProperties:"Category");
            var user = _authService.GetAuthenticatedUser();

            return new SuccessDataResult<List<Product>>(_productDal.GetList(p => p.CategoryId == categoryId).ToList());
        }

        //bu method transaction işlemleri kontrol etmek için kullanılıyor, algoritma anlamsız.
        [TransactionScopeAspect]
        public IResult TransactionalOperation(Product product)
        {
            _productDal.Update(product);
            //_productDal.Add(product);
            return new SuccessResult(Messages.ProductUpdated);
        }

        public IResult Update(Product product)
        {
            _productDal.Update(product);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
