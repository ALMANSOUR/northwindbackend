﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Core.Entities;

namespace Core.DataAccess
{
    public interface IEntityRepository<T> where T:class,IEntity,new()
    {
        T Get(Expression<Func<T,bool>> filter);
        T Get(Expression<Func<T,bool>> filter, params Expression<Func<T, object>>[] includeProperties);
        T Get(Expression<Func<T,bool>> filter, params string[] includeProperties);
        IList<T> GetList(Expression<Func<T, bool>> filter = null);
        IList<T> GetList(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includeProperties);
        IList<T> GetList(Expression<Func<T, bool>> filter = null, params string[] includeProperties);
        void Add(T entity);
        void AddRange(List<T> entity);
        void Update(T entity);
        void Delete(T entity);
        void DeleteRange(List<T> entity);
    }
}
