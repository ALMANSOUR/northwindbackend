﻿using Core.Utilities.IoC;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Utilities.Business
{
    public class AuthenticationInformation
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public AuthenticationInformation()
        {
            _httpContextAccessor = ServiceTool.ServiceProvider.GetService<IHttpContextAccessor>();
        }

        public string GetAuthenticatedEmail()
        {
            return _httpContextAccessor.HttpContext.User.Identity.Name;
        }
    }
}
