﻿using System;
using System.Collections.Generic;
using System.Text;
using Castle.DynamicProxy;

namespace Core.Utilities.Interceptors
{

    //bu attribute nerede kullanılacağını belirtir. class üzerinde kullanılabilir dolayısıyla tm metodları etkiler, veya metodlar üzerinde ve birkaç kez kullanılabilir. bir de inherited classlar için de
    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Method,AllowMultiple = true,Inherited = true)]
    public abstract class MethodInterceptionBaseAttribute:Attribute,IInterceptor
    {
        //codelarda hem validation hem de cache hem de başka attributeler yazılacak buyüzden çalışma sırası tanımlamak için bu değişkeni kullanacaz.
        public int Priority { get; set; }

        public virtual void Intercept(IInvocation invocation)
        {
            
        }
    }
}
